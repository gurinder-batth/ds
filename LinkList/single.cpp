#include <iostream>
using namespace std;

class Node
{
public:
    int data;
    Node *next;
    // Constructor Method
    Node(int data)
    {
        this->data = data;
        this->next = NULL;
    }
};

Node *head;

void printList()
{
    Node *tmp = head;
    while (tmp != NULL)
    {
        cout << tmp->data << " -> ";
        tmp = tmp->next;
    }
    cout << "NULL" << endl;
}

void insertAtHead(int val)
{
    Node *new_node = new Node(val);
    new_node->next = head;
    head = new_node;
}

void insertAt(int val, int data)
{
    Node *new_node = new Node(data);
    Node *tmp_node = head;
    while (tmp_node->data != val)
    {
        tmp_node = tmp_node->next;
        if (tmp_node == NULL)
        {
            cout << endl
                 << "OOps! Node not found which value is : " << val << " " << endl;
            return;
        }
    }
    // 63 -> 40
    new_node->next = tmp_node->next;
    tmp_node->next = new_node;
}

void deleteTail()
{
    Node *tmp_node = head;
    while (tmp_node != NULL)
    {
        if (tmp_node->next->next == NULL)
        {
            tmp_node->next = NULL;
        }
        tmp_node = tmp_node->next;
    }
}

void deleteHead()
{
    head = head->next;
}

// 200 -> 10 -> 20 -> 30 -> 40 -> 50 -> NULL
void deleteAt(int val)
{
    if (val == head->data)
    {
        deleteHead();
        return;
    }

    Node *tmp_node = head;
    while (tmp_node != NULL)
    {
        if (tmp_node->next != NULL)
        {
            if (tmp_node->next->data == val)
            {
                //  cout<<tmp_node->data<<" DATA "<<endl;
                tmp_node->next = tmp_node->next->next;
            }
        }
        tmp_node = tmp_node->next;
    }
}

int searchIndex(int val)
{
    Node *tmp_node = head;
    int idx = 0;
    while (tmp_node != NULL)
    {   
        if(tmp_node->data == val){
            return idx;
        }
        tmp_node = tmp_node->next;
        idx++;
    }
    return NULL;
}

int main()
{
    // 10  20 30 40 50
    head = new Node(10);
    head->next = new Node(20);
    head->next->next = new Node(30);
    head->next->next->next = new Node(40);
    head->next->next->next->next = new Node(50);
    printList();
    insertAtHead(200);
    printList();
    insertAt(50, 63);
    printList();
    deleteTail();
    printList();
    deleteAt(50);
    printList();
    cout<<searchIndex(30)<<endl;
    printList();
}